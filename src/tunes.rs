extern crate rand;

use std::path::PathBuf;
use glob::glob;
use glob::GlobError;
use rand::Rng;

pub fn all() -> Vec<Result<PathBuf, GlobError>> {
    glob("static/sound/*.mp3")
        .expect("Failed to read glob pattern")
        .collect::<Vec<_>>()
}

pub fn random_tune() -> (String, String) {
    let all_files = all();

    let chosen_one = match *rand::thread_rng().choose(&all_files).unwrap() {
        Ok(ref path) => path.to_owned(),
        Err(_) => panic!(""),
    };

    let file_name = chosen_one.file_name().unwrap().to_str().unwrap().to_owned();
    let file_title = chosen_one.file_stem().unwrap().to_str().unwrap().to_owned();

    return (file_name, file_title);
}
