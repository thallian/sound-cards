const min_playtime = 1000
const fade_threshold = 2000

function preload_audio (file) {
  return new Howl({
    src: [file]
  })
}

function play_audio (sound, timeout, callback) {
  sound.play()

  if (timeout) {
    if (timeout - fade_threshold > min_playtime) {
      setTimeout(function () {
        sound.fade(sound.volume(), 0, fade_threshold)
      }, timeout - fade_threshold)
    }

    setTimeout(function () {
      sound.stop()
      callback()
    }, timeout)
  }
}
